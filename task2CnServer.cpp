#include <iostream>
#include <winsock2.h>
#include <Windows.h>
#include <string>

#pragma comment (lib, "Ws2_32.lib")

using namespace std;

const int numberOfSubjects = 4;

const short serPort = 1234; // Порт сервера

struct Student { //обработка студента
	char Name[25];
	int Rating[numberOfSubjects];
}someStudent;


struct StudentInfo { //записываем сюда результаты студента
	char Name[25];
	int studyGrade;
}studentInfo;

int GrantOrNot() //функция обработки полученной о студенте информации
{
	for (int j = 0; j < numberOfSubjects; j++)
		if (someStudent.Rating[j] <= 3)
			return 0;
	for (int i = 0; i < numberOfSubjects; i++)
		if (someStudent.Rating[i] == 4)
			return 1;
	return 2;
}


void Initialization() {
	WSADATA wsa;
	int letsTry = WSAStartup(MAKEWORD(2, 2), &wsa);
	if (letsTry != 0) {
		cout << "trying initialization failed" << endl;
		exit(1);
	}
}

SOCKET CreatingSocket() {
	SOCKET hSocket;
	hSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (hSocket == INVALID_SOCKET) {
		cout << "smth went wrong (socket error)" << endl; 
		exit(1);
	}
	return hSocket;
}

void BindingSocket(SOCKET localSock)
{

	sockaddr_in sockin;
	sockin.sin_family = AF_INET;
	sockin.sin_port = serPort;
	sockin.sin_addr.s_addr = 0;
	int letsTry = bind(localSock, (sockaddr*)&sockin, sizeof(sockin));
	if (letsTry != 0) {
		cout << "trying binding socket failed" << endl;
	}
}

SOCKET Accepting(SOCKET sock) {
	//если соединение было разрешено, на сервере создается новый socket. 
	//Этот новый socket соединяется с socket’ом клиента и все операции между сервером
	//и клиентом проводятся именно по этому socket’у. 
	//Socket, который находился в режиме прослушивания, продолжает ждать новые соединения.
	sockaddr_in clientAddress;
	int clientAddressSize = sizeof(clientAddress);
	SOCKET acceptSock = accept(sock, (sockaddr*)&clientAddress, &clientAddressSize);
	if (acceptSock == INVALID_SOCKET) {
		cout << "got error while accepting" << endl;
		return 0;
	}
	return acceptSock;
}

void SendStudentInfo(SOCKET localSock) {
	int sendInfo = send(localSock, (char*)&studentInfo, sizeof(studentInfo), 0);
	if (sendInfo == SOCKET_ERROR) {
		cout << "error while sending student" << endl;
		return;
	}
}

Student GetInfoAboutStudent(SOCKET localSock) {
	int getInfo = recv(localSock, (char*)&someStudent, sizeof(someStudent), 0);
	if (getInfo == SOCKET_ERROR) {
		cout << "Error while getting info about Student" << endl;
		strcpy_s(someStudent.Name, "ERROR");
	}
	if (getInfo == 0)
		strcpy_s(someStudent.Name, "ERROR");//копирует чаровский массив с нуль-терминатором
	
	return someStudent;
}

int main()
{
	Initialization();
	SOCKET sock = CreatingSocket();
	BindingSocket(sock);
	listen(sock, 3);
	cout << "Server started!" << endl;
	while (true) {
		SOCKET acceptSock;
		acceptSock = Accepting(sock);
		while (true) {
			someStudent = GetInfoAboutStudent(acceptSock);
			if (strcmp(someStudent.Name, "ERROR") == 0) {//сравниваем символы двух строк
				cout << "Break"; break;
			}
			strcpy_s(studentInfo.Name, someStudent.Name);
			studentInfo.studyGrade = GrantOrNot();
			SendStudentInfo(acceptSock);
		}
		closesocket(acceptSock);
	}
}