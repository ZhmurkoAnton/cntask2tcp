// TCPClient.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
#include <iostream>  
#include <winsock2.h> 
#include <string>
#include <windows.h>

#define _WINSOCK_DEPRECATED_NO_WARNINGS  // подавление предупреждений библиотеки winsock2

#pragma comment (lib, "Ws2_32.lib")
#pragma warning(disable: 4996)  // подавление предупреждения 4996 
using namespace std;
const short serverPort = 1234; // Порт сервера
const short clientPort = 1235; // Порт клиента
#define SRV_HOST "localhost"  

const int numberOfSubjects = 4;

struct Student { //обработка студента
	char name[25];
	int grades[numberOfSubjects];
}someStudent;

struct StudentInfo { //струткура-ответ с реузльтатами студента
	char name[25];
	int studyGrade;
}studentInfo;
void Initialization() {// инициализировать библиотеку Ws2_32.dll.
	WSADATA wsa;
	int letsTry = WSAStartup(MAKEWORD(2, 2), &wsa);
	//Максимальная версия Windows Socket, которую может использовать вызывающая программа.
	//Старший байт содержит младшую часть номера версии, младший байт содержит старшую часть номера версии.
	//,Указатель на структуру WSADATA, которая, в результате выполнения функции, будет содержать детали реализации Windows Sockets.
	if (letsTry != 0) {
		cout << "Initialization error" << endl;
		exit(1);
	}
}

SOCKET CreatingSocket() {//создает новый socket и возвращает его дескриптор
	SOCKET hSocket;
	hSocket = socket(AF_INET, SOCK_STREAM, 0);
	//так называемое, адресное семейство -> значение AF_INET, - TCP и UDP «семейство»
	//,созданиe потокового socket’а(Потоковый вариант разработан для приложений, нуждающихся в надежном соединении и часто использующем продолжительные потоки данных)
	//,протокол, который будет использоваться socket’ом
	if (hSocket == INVALID_SOCKET) {
		cout << "smth went wrong (socket error)" << endl; 
		exit(1);
	}
	return hSocket;//Тип этого дескриптора SOCKET, и он используется во всех функциях, работающих с socket’ами.
}

void BindingSocket(SOCKET locakSock)//Связываем socket (т.е."прикрепляем" определенный адрес (IP адрес и номер порта)) к данному socket’у. 
{
	sockaddr_in sockin;//Cтруктура sockaddr_in описывает сокет для работы с протоколами IP.
	sockin.sin_family = AF_INET;// всегда равно AF_INET
	sockin.sin_port = clientPort;//содержит номер порта который намерен занять процесс.
	//Если значение этого поля равно нулю, то операционная система сама выделит свободный номер порта для сокета
	sockin.sin_addr.s_addr = 0;//содержит IP адрес к которому будет привязан сокет(в данном случае все адреса локального хоста )
	int letsTry = bind(locakSock, (sockaddr*)&sockin, sizeof(sockin));
	//Связать socket значит «прикрепить» определенный адрес (IP адрес и номер порта) к данному socket’у.
	if (letsTry != 0) {
		cout << "trying binding socket failed" << endl;
		exit(1);
	}
}

void Connection(SOCKET sock) {
	hostent* hp;//Структура hostent используется функциями, чтобы хранить информацию о хосте: его имя, тип, IP адрес, и т.д.
	hp = gethostbyname(SRV_HOST);//Получает информацию о хосте по его имени. Результат работы помещается в специальную структуру hostent
	sockaddr_in connectionSockin;// является TCP/IP версией структуры sockaddr
	connectionSockin.sin_family = AF_INET;// Устанавливаем адресное семейство
	/*sin_port – это 16-битовый номер порта*/connectionSockin.sin_port = serverPort;// Преобразуем номер порта в сетевой порядок байтов
	((unsigned long*)&connectionSockin.sin_addr)[0] =
		((unsigned long**)hp->h_addr_list)[0][0];
	//Функция connect соединят socket с удаленным socket’ом. Эта функция используется на клиентской стороне
	//подключения, т.к. именно клиент является инициатором подключения
	int letsTry = connect(sock, (sockaddr*)&connectionSockin, sizeof(connectionSockin));
	// sock - неподключенный socket, который я хочу подключить
	//,указатель на структуру sockaddr, в которой содержится имя (адрес) удаленного socket’а, к которому необходимо подключится.
	//,размер структуры, в которой содержится имя
	if (letsTry != 0) {
		cout << "Connection error " << endl;
		exit(1);
	}
}

void SendStudentInfo(SOCKET sock) {
	int sendInfo = send(sock, (char*)&someStudent, sizeof(someStudent), 0);
	//отправляет данные из буфера и возвращает количество отправленных байт
	//sock-подключенный socket, для передачи данных
	//,буфер, содержащий данные для отправки
	//,размер данных для передачи
	//, определяет способ передачи данных
	if (sendInfo == SOCKET_ERROR) {
		cout << "Error while sending info about student" << endl;
		exit(1);
	}
//	cout << "send info about student successful\n";
}

StudentInfo GetInfoAboutStudent(SOCKET sock) {
	StudentInfo oneStudent;
	int getInfo = recv(sock, (char*)&oneStudent, sizeof(oneStudent), 0);
	//функция получает данные и сохраняет их в буфер
	//sock-подключенный socket, для получения данных
	//,буфер, в котором будут храниться полученные данные
	//размер полученных данных
	//определяет способ получения данных
	if (getInfo == SOCKET_ERROR) {
		cout << "Error while getting info about Student" << endl;
		exit(1);
	}
//	cout << "Get info about student successful" << endl;
	return oneStudent;
}

int main()
{
	Initialization();//Перед вызовом любой WinSock функции инициализирую библиотеку Ws2_32.dll
	SOCKET sock = CreatingSocket();//реальные данные сокета хранятся внутри винды а мне выдается просто число - 
	//номер этого сокета в моей программе(это и есть дескриптор)
	BindingSocket(sock);//т.е."прикрепляем" определенный адрес (IP адрес и номер порта)) к sock.
	Connection(sock);//подключаемся по sock
	int dixi;
	while (true) {
		cout << "To stop entering students enter 0,else enter some digit\n";
		cin >> dixi;
		if (dixi == 0)
			return 0;
		else {
			cout << "Enter student name: ";
			cin >> someStudent.name;
			cout << "Input grades: ";
			for (int i = 0; i < numberOfSubjects; i++)
				cin >> someStudent.grades[i];
			SendStudentInfo(sock);//отправляет данные о студенте на сервер
			////получает инфу о студенте(структурой)
			switch (GetInfoAboutStudent(sock).studyGrade)//наонец-то узнаем о наличии или отсутствии стипендии у студента
			{
			case 0:
				cout << "\nno grant\n\n";
				break;
			case 1:
				cout << "\nOrdinary grant\n\n";
				break;
			case 2:
				cout << "\nIncreased  grant\n\n";
				break;
			}
		}
		//Каждый созданный socket с помощью функции socket(), должен быть закрыт с помощью функции closesocket().
		closesocket(sock);//принимает единственный параметр - дескриптор socket’а, который необходимо закрыть
	}
}
